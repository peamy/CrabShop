using System;
using System.Collections.Generic;

namespace crabshop.Models
{
    public class Order : BaseEntity
    {
        public Guid Id { get; set; }
        public float TotalPrice { get; set; }
        public IEnumerable<OrderedProduct> Products { get; set; }
        public Guid? Userid { get; set; }
    }
}