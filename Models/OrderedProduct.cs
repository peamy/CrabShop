using System;

namespace crabshop.Models
{
    public class OrderedProduct : BaseEntity
    {
        public Guid Id { get; set; }

        public Product Product { get; set; }

        public int Amount { get; set; }

        public float Price { get; set; }
    }
}