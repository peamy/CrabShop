using System;

namespace crabshop.Models
{
    public class Product : BaseEntity
    {
        public Guid Id { get; set; }
       
        public String Name { get; set; }

        public decimal Price { get; set; } 

        public String ImageUrl { get; set; }
    }
}
