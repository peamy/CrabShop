using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace crabshop.Models
{
    [NotMapped]
    public class UserRole
    {
        public const string Customer = "Customer";
        public const string Admin = "Admin";
    }

    public class User : BaseEntity
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; } // is of UserRole.
        public IEnumerable<Order> Orders { get; set; }
    }
}