using System;

namespace crabshop.Models
{
    public class BaseEntity
    {
        public DateTimeOffset CreatedDate { get; set; }

        public DateTimeOffset LastUpdated { get; set; }
    }
}