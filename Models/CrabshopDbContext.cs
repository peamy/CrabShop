using System.Collections.Generic;
using crabshop.Models;
using Microsoft.EntityFrameworkCore;

namespace crabshop.Models
{
    public class CrabshopDbContext : DbContext
    {
        public CrabshopDbContext(DbContextOptions<CrabshopDbContext> options) : base(options) { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderedProduct> OrderedProducts { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
