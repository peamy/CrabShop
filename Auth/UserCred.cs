namespace crabshop.Auth
{
    public class UserCred
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}