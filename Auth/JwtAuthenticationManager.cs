using System;
using System.Collections.Generic;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Security.Claims;
using crabshop.Models;
using crabshop.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace crabshop.Auth
{
    public class JwtAuthenticationManager : IJwtAuthenticationManager
    {
        private readonly IUserService _userService;
        private readonly string key;
        private readonly PasswordHasher<User> _passwordHasher;
        private readonly ILogger<JwtAuthenticationManager> _logger;

        public JwtAuthenticationManager(IUserService userService, ILogger<JwtAuthenticationManager> logger, IConfiguration config)
        {
            _passwordHasher = new PasswordHasher<User>();
            _userService = userService;
            _logger = logger;
            key = config.GetValue<string>("Keys:JwtKey");
        }
        public string Authenticate(string email, string password)
        {
            User user = _userService.GetByEmail(email);

            if (user is null)
            {
                return null;
            }

            if (_passwordHasher.VerifyHashedPassword(user, user.Password, password) != PasswordVerificationResult.Success)
            {
                _logger.LogInformation($"Failed authentication attempt for user: {email}");
                return null;
            }

            _logger.LogInformation($"Successfully authenticated user: {email}");
            JwtSecurityTokenHandler tokenHandler  = new JwtSecurityTokenHandler();
            byte[] tokenKey = Encoding.ASCII.GetBytes(key);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, email),
                    new Claim(ClaimTypes.Role, UserRole.Customer)
                }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}