namespace crabshop.Auth
{
    public interface IJwtAuthenticationManager
    {
        // Returns a created JWT token.
        string Authenticate(string user, string password);
    }
}