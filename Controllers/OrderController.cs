using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using crabshop.Models;
using crabshop.Services;
using crabshop.Dtos;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace crabshop.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly ILogger<OrderController> _logger;

        public OrderController(ILogger<OrderController> logger, IOrderService orderService)
        {
            _logger = logger;
            _orderService = orderService;
        }

        [HttpGet]
        public ActionResult<OrderDto[]> Find()
        {
            _logger.LogInformation($"[{DateTime.UtcNow.ToLongTimeString()}] GetAll");
            IEnumerable<OrderDto> orders = _orderService.Find();

            if (orders is null)
            {
                return NotFound();
            }

            return Ok(orders);
        }

        [HttpGet("{id}")]
        public ActionResult<OrderDto> GetById(Guid id)
        {
            _logger.LogInformation($"[{DateTime.UtcNow.ToLongTimeString()}] GetById - Finding Order with id: [{id}]");
            Order order = _orderService.GetById(id);

            if (order is null)
            {
                return NotFound();
            }

            return Ok(order.AsDto());
        }

        [HttpGet("/mine")]
        public ActionResult<IEnumerable<OrderDto>> GetMyOrders()
        {
            string userEmail = User.Identity.Name;
            _logger.LogInformation($"{DateTime.UtcNow.ToLongTimeString()} GetMyOrders - Finding Order for user with email {userEmail}");

            IEnumerable<Order> orders = _orderService.GetMyOrders(userEmail);

            if (orders is null)
            {
                return NotFound();
            }

            _logger.LogInformation($"{orders}");

            return Ok(orders.Select( order => order.AsDto()).ToList());
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult<OrderDto> CreateOrder(OrderCreateDto orderDto)
        {
            string userEmail = User.Identity.Name; // If the request was made by an authenticated user, this value contains that users Email address, otherwise it's null.
            Order order = _orderService.CreateOrder(orderDto, userEmail);

            if (order is null)
            {
                return BadRequest();
            }

            return Ok(order.AsDto());
        } 
    }
}