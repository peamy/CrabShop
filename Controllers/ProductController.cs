using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using crabshop.Models;
using crabshop.Dtos;
using crabshop.Services;
using Microsoft.AspNetCore.Authorization;

namespace crabshop.Controllers
{
    [Authorize(Roles = UserRole.Admin)]
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductService _productService;

        public ProductController(ILogger<ProductController> logger, IProductService productService)
        {
            _logger = logger;
            _productService = productService;
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult<ProductDto[]> GetAll()
        {
            _logger.LogInformation($"[{DateTime.UtcNow.ToLongTimeString()}] GetAll");
            IEnumerable<ProductDto> products = _productService.Find();

            if (products is null) {
                return NotFound();
            }

            return Ok(products);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public ActionResult<ProductDto> GetById(Guid id)
        {
            Product product = _productService.GetById(id);

            if (product is null) {
                return NotFound();
            }

            return Ok(product.AsDto());
        }

        [HttpPut("{id}")]
        public ActionResult<ProductDto> UpdateProduct(Guid id, ProductUpdateDto productDto)
        {
            Product product = _productService.UpdateProduct(id, productDto);

            if (product is null)
            {
                return NotFound();
            }

            return Ok(product.AsDto());
        }

        [HttpPost]
        public ActionResult<ProductDto> CreateProduct(ProductCreateDto productDto)
        {
            Product product = _productService.CreateProduct(productDto);

            if (product is null)
            {
                return BadRequest();
            }

            return Ok(product.AsDto());
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(Guid id)
        {
            Product product = _productService.DeleteProduct(id);

            if (product is null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
