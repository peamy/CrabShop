using Microsoft.AspNetCore.Mvc;
using crabshop.Auth;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using crabshop.Models;

namespace crabshop.Controllers
{
    [Authorize(Roles = UserRole.Customer)]
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IJwtAuthenticationManager jwtAuthenticationManager;

        public AuthController(IJwtAuthenticationManager jwtAuthenticationManager)
        {
            this.jwtAuthenticationManager = jwtAuthenticationManager;
        }

        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { User.Identity.Name };
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] UserCred userCred)
        {
            string token = jwtAuthenticationManager.Authenticate(userCred.Email, userCred.Password);

            if (token is null)
            {
                return Unauthorized();
            }

            return Ok(token);
        }
    }
}