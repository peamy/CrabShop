using System;
using System.Collections.Generic;
using crabshop.Dtos;
using crabshop.Models;
using crabshop.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace crabshop.Controllers
{
    [Authorize(Roles = UserRole.Admin)]
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        public UserController(ILogger<UserController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<UserDto[]> Find()
        {
            IEnumerable<UserDto> users = _userService.Find();

            if (users is null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        [HttpGet("{id}")]
        public ActionResult<UserDto> GetById(Guid id)
        {
            User user = _userService.GetById(id);

            if (user is null)
            {
                return NotFound();
            }

            return Ok(user.AsDto());
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult<UserDto> CreateUser(UserCreateDto user)
        {
            User newUser = _userService.CreateUser(user);

            if (newUser is null)
            {
                return BadRequest();
            }

            return Ok(newUser.AsDto());
        }
    }
}