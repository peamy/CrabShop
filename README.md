## Crabshop

An API that handles orders for image of Crabs.
Written in .net.

#### Run the project

Open the project in VScode, go to the Run and Debug tab, and click start.

#### Create database

```
dotnet ef migrations add newMigrationName
```

#### Update database

```
dotnet ef database update newMigrationName
```

#### Build

```
dotnet build
```

