using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace crabshop.Dtos
{
    public record OrderDto
    {
        [Required]
        public Guid Id { get; init; }

        [Required]
        public float TotalPrice { get; init; }

        [Required]
        public IEnumerable<OrderedProductDto> Products { get; init; }
    }
}