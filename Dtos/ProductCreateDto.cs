using System;
using System.ComponentModel.DataAnnotations;

namespace crabshop.Dtos
{
    public record ProductCreateDto
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public String Name { get; init; }

        [Required]
        [Range(0, 10000)]
        public decimal Price { get; init; } 

        [Required]
        [StringLength(254, MinimumLength = 10)]
        public String ImageUrl { get; init; }
    }
}