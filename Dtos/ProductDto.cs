using System;

namespace crabshop.Dtos
{
    public record ProductDto
    {
        public Guid Id { get; init; }
       
        public String Name { get; init; }

        public decimal Price { get; init; } 

        public String ImageUrl { get; init; }
    }
}
