using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace crabshop.Dtos
{
    public record OrderCreateDto
    {
        [Required]
        public float TotalPrice { get; init; }

        [Required]
        public OrderedProductCreateDto[] Products { get; init; }
    }
}