using System;
using System.ComponentModel.DataAnnotations;

namespace crabshop.Dtos
{
    public record OrderedProductCreateDto
    {
        [Required]
        public Guid ProductId { get; init; }

        [Required]
        public int Amount { get; init; }

        [Required]
        public float Price { get; init; }
    }
}