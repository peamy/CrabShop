using System;
using System.ComponentModel.DataAnnotations;

namespace crabshop.Dtos
{
    public record OrderedProductDto
    {
        [Required]
        public Guid Id { get; init; }

        [Required]
        public ProductDto Product { get; init; }

        [Required]
        public int Amount { get; init; }

        [Required]
        public float Price { get; init; }
    }
}