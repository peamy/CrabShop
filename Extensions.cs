using crabshop.Dtos;
using crabshop.Models;
using System.Linq;

namespace crabshop
{
    public static class Extensions
    {
        public static ProductDto AsDto(this Product product)
        {
            return new ProductDto
            {
                Id = product.Id,
                Name = product.Name,
                Price = product.Price,
                ImageUrl = product.ImageUrl,
            };
        }

        public static OrderDto AsDto(this Order order)
        {
            return new OrderDto
            {
                Id = order.Id,
                TotalPrice = order.TotalPrice,
                Products = order.Products.Select( OrderedProduct => OrderedProduct.AsDto()).ToList(),
            };
        }

        public static OrderedProductDto AsDto(this OrderedProduct orderedProduct)
        {
            return new OrderedProductDto
            {
                Id = orderedProduct.Id,
                Product = orderedProduct.Product.AsDto(),
                Amount = orderedProduct.Amount,
                Price = orderedProduct.Price
            };
        }

        public static UserDto AsDto(this User user)
        {
            return new UserDto
            {
                Email = user.Email
            };
        }
    }
}