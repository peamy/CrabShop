using System;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using crabshop.Dtos;
using crabshop.Models;
using Microsoft.AspNetCore.Identity;

namespace crabshop.Services
{
    public interface IUserService
    {
        IEnumerable<UserDto> Find();
        User GetById(Guid id);
        User GetByEmail(string email);
        User CreateUser(UserCreateDto user);
    }
    public class UserService : IUserService
    {
        private readonly ILogger _logger;
        private readonly CrabshopDbContext _db;
        private readonly PasswordHasher<User> _passwordHasher;

        public UserService(ILogger<UserService> logger, CrabshopDbContext db)
        {
            _logger = logger;
            _db = db;
            _passwordHasher = new PasswordHasher<User>();
        }

        public IEnumerable<UserDto> Find()
        {
            try {
                IEnumerable<UserDto> users = (IEnumerable<UserDto>) _db.Users.ToList().Select( user => user.AsDto());
                return users;
            }
            catch (Exception e)
            {
                _logger.LogError($"{DateTime.UtcNow.ToLongTimeString()} Error fetching all Users from database -> {e}");
                return null;
            }
        }

        public User GetById(Guid id)
        {
            try {
                User user = _db.Users.Find(id);
                return user;
            }
            catch(Exception e) {
                _logger.LogError($"{DateTime.UtcNow.ToLongTimeString()} Error trying to fetch User with id {id} from database -> {e}");
                return null;
            }
        }

        public User GetByEmail(string email)
        {
            try {
                User user = _db.Users.Where( u => u.Email == email).First();
                return user;
            }
            catch(Exception e) {
                _logger.LogError($"{DateTime.UtcNow.ToLongTimeString()} Error trying to fetch User with email {email} from database -> {e}");
                return null;
            }
        }

        public User CreateUser(UserCreateDto user)
        {
            _logger.LogInformation($"{DateTime.UtcNow.ToLongTimeString()} Creating a new user: {user.Email}");

            User newUser = new();
            newUser.Id = Guid.NewGuid();
            newUser.Email = user.Email;
            newUser.Password = _passwordHasher.HashPassword(newUser, user.Password);
            newUser.Role = UserRole.Customer;
            newUser.CreatedDate = DateTimeOffset.UtcNow;
            newUser.LastUpdated = DateTimeOffset.UtcNow;

            _db.Add(newUser);

            try {
                var changes = _db.SaveChanges();
                return this.GetById(newUser.Id);
            }
            catch(Exception e)
            {
                _logger.LogError($"{DateTime.UtcNow.ToLongTimeString()} Error when trying to save new user {user.Email} to database -> {e}");
                return null;
            }
        }
    }
}