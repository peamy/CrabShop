using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using crabshop.Dtos;
using crabshop.Models;
using Microsoft.EntityFrameworkCore;

namespace crabshop.Services
{
    public interface IOrderService
    {
        IEnumerable<OrderDto> Find();
        Order GetById(Guid id);
        IEnumerable<Order> GetMyOrders(string userEmail);
        Order CreateOrder(OrderCreateDto orderDto, string userEmail = "");
    }

    public class OrderService : IOrderService
    {
        private readonly CrabshopDbContext _db;
        private readonly ILogger<OrderService> _logger;
        private readonly IProductService _productService;
        private readonly IUserService _userService;

        public OrderService(ILogger<OrderService> logger, CrabshopDbContext db, IProductService productService, IUserService userService)
        {
            _db = db;
            _logger = logger;
            _productService = productService;
            _userService = userService;
        }

        public IEnumerable<OrderDto> Find()
        {
            try {
                IEnumerable<OrderDto> orders = _db.Orders.Include( order => order.Products).ThenInclude( orderedProduct => orderedProduct.Product).Select( order => order.AsDto()).ToList();
                return orders;
            }
            catch(Exception e) {
                _logger.LogError($"Error trying fetch all orders from database -> [{e}]");
                return null;
            }
        }

        public Order GetById(Guid id)
        {
            try {
                Order order = _db.Orders.Include( order => order.Products).ThenInclude( orderedProducts => orderedProducts.Product).FirstOrDefault( order => order.Id == id);
                return order;
            }
            catch(Exception e) {
                _logger.LogError($"Error trying fetch order with id [{id}] from database -> [{e}]");
                return null;
            }
        }
        
        public IEnumerable<Order> GetMyOrders(string userEmail)
        {
            User user = _userService.GetByEmail(userEmail);

            if (user is null)
            {
                return null;
            }

            try {
                IEnumerable<Order> orders = _db.Orders.Include( order => order.Products).ThenInclude( orderedProduct => orderedProduct.Product).Where( order => order.Userid == user.Id).ToList();
                return orders;
            }
            catch(Exception e)
            {
                _logger.LogError($"{DateTime.UtcNow.ToLongTimeString()} Error fetching orders for user with email {userEmail} -> {e}");
                return null;
            }
        }

        public Order CreateOrder(OrderCreateDto orderDto, string userEmail = "")
        {
            // If the request was made by an authenticated user, and userEmail is filled, we fetch the user so we can couple the order to their account. 
            User user = _userService.GetByEmail(userEmail);

            try {
                Order order = new()
                {
                    Id = Guid.NewGuid(),
                    TotalPrice = orderDto.TotalPrice,
                    Products = orderDto.Products.Select( OrderedProduct => new OrderedProduct {
                        Id = Guid.NewGuid(),
                        Amount = OrderedProduct.Amount,
                        Price = OrderedProduct.Price,
                        Product = _productService.GetById(OrderedProduct.ProductId),
                        CreatedDate = DateTimeOffset.UtcNow,
                        LastUpdated = DateTimeOffset.UtcNow
                    }).ToList(),
                    Userid = user is null ? null : user.Id, // It's possible that this value is null. An order can be made anonymously.
                    CreatedDate = DateTimeOffset.UtcNow,
                    LastUpdated = DateTimeOffset.UtcNow
                };

                _db.Orders.Add(order);
                _db.SaveChanges();

                return this.GetById(order.Id);
            }
            catch(Exception e) {
                _logger.LogError($"Error creating now Order: [{e}]");
                return null;
            }
        }
    }
}