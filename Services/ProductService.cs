using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using crabshop.Dtos;
using crabshop.Models;

namespace crabshop.Services
{
    public interface IProductService
    {
        IEnumerable<ProductDto> Find();
        Product GetById(Guid id);
        Product UpdateProduct(Guid id, ProductUpdateDto productDto);
        Product CreateProduct(ProductCreateDto productDto);
        Product DeleteProduct(Guid id);
    }

    public class ProductService : IProductService
    {
        private readonly CrabshopDbContext _db;
        private readonly ILogger<ProductService> _logger;

        public ProductService(ILogger<ProductService> logger, CrabshopDbContext db)
        {
            _logger = logger;
            _db = db;
        }

        public IEnumerable<ProductDto> Find()
        {
            try {
                IEnumerable<ProductDto> products = (IEnumerable<ProductDto>) _db.Products.ToList().Select( product => product.AsDto());
                return products;
            }
            catch(Exception e) {
                _logger.LogError($"[{DateTime.UtcNow.ToLongTimeString()}] Error trying fetch all products from database -> [{e}]");
                return null;
            }
        }

        public Product GetById(Guid id)
        {
            Product product;
            
            try {
                product = _db.Products.Find(id);
                return product;
            }
            catch(Exception e) {
                _logger.LogError($"Error trying fetch product with id [{id}] from database -> [{e}]");
                return null;
            }
        }

        public Product UpdateProduct(Guid id, ProductUpdateDto productDto)
        {
            _logger.LogInformation($"[{DateTime.UtcNow.ToLongTimeString()}] Updating product with Id: [{id}]");
            Product product = this.GetById(id);

            if (product is null)
            {
                _logger.LogInformation($"[{DateTime.UtcNow.ToLongTimeString()}] Product with Id: [{id}] - not found. Not updating product.");
                return null;
            }

            product.Name = productDto.Name;
            product.Price = productDto.Price;
            product.ImageUrl = productDto.ImageUrl;
            product.LastUpdated = DateTimeOffset.UtcNow;

            try {
                var changes = _db.SaveChanges();
            }
            catch(Exception e) {
                _logger.LogError($"Error trying to update product with id: [{id}], in database -> [{e}]");
                return null;
            }

            return this.GetById(product.Id);
        }

        public Product CreateProduct(ProductCreateDto productDto)
        {
            _logger.LogInformation($"[{DateTime.UtcNow.ToLongTimeString()}] Creating a new product: [{productDto.ToString()}]");

            Product newProduct = new()
            {
                Id = Guid.NewGuid(),
                Name = productDto.Name,
                Price = productDto.Price,
                ImageUrl = productDto.ImageUrl,
                CreatedDate = DateTimeOffset.UtcNow,
                LastUpdated = DateTimeOffset.UtcNow
            };

            _db.Products.Add(newProduct);

            try {
                var changes = _db.SaveChanges();
                return this.GetById(newProduct.Id);
            }
            catch(Exception e) {
                _logger.LogError($"Error trying to store new product in database -> [{e}]");
                return null;
            }
        }

        public Product DeleteProduct(Guid id)
        {
             _logger.LogError($"Removing product with id [{id}] from the database.");
            Product product = this.GetById(id);

            if (product is null)
            {
                return null;
            }

            _db.Products.Remove(product);

            try {
                var changes = _db.SaveChanges();
                return product;
            }
            catch(Exception e) {
                _logger.LogError($"Error removing product with id [{id}] from the database: -> [{e}]");
                return null;
            }
        }
    }
}